var turno = "X";
var totalJugadas = 0;

//Punto de entrada a la pagina
function iniciarJuego() {
    // Crear 9 botones

    let miBoton;
    let formulario;
    let i;
    let botonReiniciar;

    //Crear un objeto que referencie al formulario
    formulario = document.getElementById("formTablero");



    for (i = 1; i < 10; i++) {
        //Agregar un elemento input
        miBoton = document.createElement("input");

        //Agregar algunos atributos al objeto
        miBoton.setAttribute("type", "button");
        miBoton.setAttribute("id", "boton" + i);
        miBoton.setAttribute("class", "boton");
        miBoton.setAttribute("value", " ");
        miBoton.setAttribute("onclick", "realizarJugada(this.id)");
        formulario.appendChild(miBoton);

        if (i % 3 == 0) {
            salto = document.createElement("br");
            formulario.appendChild(salto);
        }
    }


    botonReiniciar = document.createElement("input");
    botonReiniciar.setAttribute("type", "button");
    botonReiniciar.setAttribute("id", "botonReiniciar");
    botonReiniciar.setAttribute("value", "Reiniciar");
    botonReiniciar.setAttribute("onclick", "borrarTriqui()");


    formulario.appendChild(botonReiniciar);


}

function realizarJugada(elemento) {
    let triqui;
    let resultado = marcarCasilla(elemento);

    if (resultado == true) {
        totalJugadas++;
        triqui = calcularTriqui();
        if (triqui) {
            alert("Has ganado");
            borrarTriqui();
        } else if (totalJugadas == 9) {
            alert("Empate");
            borrarTriqui();
        } else {
            if (turno === "X") {
                turno = "O";
            } else {
                turno = "X";
            }
        }

    }
}

function marcarCasilla(elemento) {
    //mensaje de casilla marcada
    let casilla = document.getElementById(elemento);

    if (casilla.value === "X" || casilla.value === "O") {
        alert("Casilla marcada.");
        return false;
    }

    casilla.value = turno;

    return true;
}

function calcularTriqui() {
    let i;
    let casilla1, casilla2, casilla3;
    //Triqui en filas
    for (i = 0; i < 9; i = i + 3) {
        casilla1 = document.getElementById("boton" + (i + 1)).value;
        casilla2 = document.getElementById("boton" + (i + 2)).value;
        casilla3 = document.getElementById("boton" + (i + 3)).value;
        if (casilla1 === casilla2 && casilla1 === casilla3 && casilla1 != " ") {
            return true;
        }
    }
    //triqui en columnas
    for (i = 1; i < 4; i++) {
        casilla1 = document.getElementById("boton" + i).value;
        casilla2 = document.getElementById("boton" + (i + 3)).value;
        casilla3 = document.getElementById("boton" + (i + 6)).value;
        if (casilla1 === casilla2 && casilla1 === casilla3 && casilla1 != " ") {
            return true;
        }
    }
    //triqui en diagonal
    let diagonal = [2, 4];
    for (i = 0; i < 2; i++) {
        casilla1 = document.getElementById("boton" + 5).value;
        casilla2 = document.getElementById("boton" + (5 + diagonal[i])).value;
        casilla3 = document.getElementById("boton" + (5 - diagonal[i])).value;
        if (casilla1 === casilla2 && casilla1 === casilla3 && casilla1 != " ") {
            return true;
        }
    }
    return false;
}

function borrarTriqui() {
    //borrar tablero
    totalJugadas = 0;
    turno = "X";
    for (i = 1; i < 10; i++) {
        boton = document.getElementById("boton" + i);
        boton.value = " ";
    }
}